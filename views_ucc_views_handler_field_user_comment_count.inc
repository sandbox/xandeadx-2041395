<?php

class views_ucc_views_handler_field_user_comment_count extends views_handler_field_numeric {
  function query() {
    $this->ensure_my_table();
    
    $this->field_alias = $this->query->add_field(
      NULL,
      '(SELECT COUNT(*) FROM {comment} c WHERE c.uid = users.uid AND c.status = 1)',
      'user_comment_count'
    );
    
    $this->add_additional_fields();
  }
}
